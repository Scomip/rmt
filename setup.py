from setuptools import setup

setup(
    name='rmt',
    version='0.2',
    packages=['rmt', 'rmt_async'],
    entry_points={
        'console_scripts': [
            'rmt = rmt.cli:run_rmt',
            'rmtrm = rmt.cli:run_rmtrm'
        ]
    },
    tests_require=['pytest', 'pyfakefs']
)
