import time

import pytest

import rmt.trashcan
import rmt_async.trashcan


@pytest.fixture
def can(request, tmpdir):
    return rmt_async.trashcan.AsyncTrashcan(str(tmpdir.join(".trash")))


def _fetch_one(can):
    for _ in xrange(3):
        time.sleep(0.1)
        fetched_result = can.fetch()
        if fetched_result is not None:
            break
    else:
        pytest.fail("failed to fetch result from trashcan")
    return fetched_result


def test_creation(can, tmpdir):
    assert tmpdir.join(".trash", "contents.json").check()


def test_put_file(can, tmpdir):
    tmpdir.join("file").write("")
    can.put(str(tmpdir.join("file")))
    _, result = _fetch_one(can)
    assert isinstance(result, rmt.trashcan.TrashRecord)
    assert not tmpdir.join("file").check()
    assert tmpdir.join(".trash", "1").check()
    assert can.count() == 1
    assert result == can.get(1)
    assert result.path == tmpdir.join("file")


def test_put_nonexistent(can, tmpdir):
    can.put(str(tmpdir.join("file")))
    _, result = _fetch_one(can)
    assert isinstance(result, OSError)


def test_erase_file(can, tmpdir):
    tmpdir.join("file").write("")
    can.put(str(tmpdir.join("file")))
    _fetch_one(can)
    can.erase(1)
    _, result = _fetch_one(can)
    assert isinstance(result, rmt.trashcan.TrashRecord)
    assert not tmpdir.join("file").check()
    assert not tmpdir.join(".trash", "1").check()
    assert can.count() == 0


def test_restore_file(can, tmpdir):
    tmpdir.join("file").write("")
    can.put(str(tmpdir.join("file")))
    _fetch_one(can)
    can.restore(1)
    _, result = _fetch_one(can)
    assert isinstance(result, rmt.trashcan.TrashRecord)
    assert tmpdir.join("file").check()
    assert not tmpdir.join(".trash", "1").check()
    assert can.count() == 0


def test_items(can, tmpdir):
    tmpdir.join("file1").write("")
    tmpdir.join("file2").write("")
    can.put(str(tmpdir.join("file1")))
    can.put(str(tmpdir.join("file2")))
    _fetch_one(can)
    _fetch_one(can)
    items_list = list(can.items())
    assert len(items_list) == 2
    assert items_list[0].record_id == 1
    assert items_list[1].record_id == 2
