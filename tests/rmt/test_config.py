import datetime
import os.path

import pytest

import rmt.config


@pytest.mark.parametrize("param", [
    ("i_config_bad_line", "can`t identify a line"),
    ("i_config_double_key", "config key 'verbose' already met"),
    ("i_config_end", "expecting end of section"),
    ("i_double_section", "section 'trashcan' already met"),
    ("i_global_bad_line", "can`t identify a line"),
    ("i_politics_bad_line", "can`t identify a line"),
    ("i_politics_double", "politic 'name' already met"),
    ("i_politics_end", "expecting end of section"),
    ("i_trashcan_double", "trashcan section must contain at most one line"),
    ("i_trashcan_end", "expecting end of section"),
    ("i_reserved_politic", "politic 'all' is reserved")
])
def test_parse_invalid(datadir, param):
    (filename, right_msg) = param
    config_path = os.path.join(datadir, filename)
    with open(config_path) as config_file:
        with pytest.raises(ValueError) as exc_info:
            rmt.config.parse_custom_config(config_file)
    assert exc_info.value.message == right_msg


def test_parse_valid(datadir):
    config_path = os.path.join(datadir, "valid")
    with open(config_path) as config_file:
        result = rmt.config.parse_custom_config(config_file)
    assert result == {
        'trashcan': '~/.trash',
        'politics': {
            'old': [
                (datetime.timedelta(days=30), 5*1024),
                (datetime.timedelta(seconds=5*60+1), 10*(1024**2))
            ]
        },
        'config': {
            'verbose': False,
            'remove_interactive': 'always'
        }
    }


def test_validate_config():
    with pytest.raises(ValueError):
        rmt.config.validate_config({"bad_key": "some_value"})
    with pytest.raises(ValueError):
        rmt.config.validate_config({"remove_interactive": "bad_value"})
    with pytest.raises(TypeError):
        rmt.config.validate_config({"silent": "bad_value"})


def test_translate():
    with pytest.raises(ValueError):
        rmt.config.translate("bad", ("good", "even_better"))
    with pytest.raises(ValueError):
        rmt.config.translate(42, bool)
    assert rmt.config.translate("string", basestring) == "string"
    with pytest.raises(ValueError):
        rmt.config.translate(42, int)
