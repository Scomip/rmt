import __builtin__
import os

from collections import deque

import pytest


@pytest.fixture
def datadir(tmpdir, request):
    filename = request.module.__file__
    test_dir, _ = os.path.splitext(filename)
    return test_dir


@pytest.fixture
def emu_in(monkeypatch):
    class Emulator(object):
        def __init__(self):
            self.messages = deque()

        def _in(self, *args):
            if self.messages:
                return self.messages.pop()
            else:
                return ""

        def send(self, msg):
            self.messages.append(msg)
    emu = Emulator()
    monkeypatch.setattr(__builtin__, "raw_input", emu._in)
    return emu
