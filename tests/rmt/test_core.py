import os
import stat

import pytest

import rmt.core


@pytest.fixture
def session(request, fs):
    session = rmt.core.TrashcanSession(trashcan_path="/.trash")

    def teardown():
        session.close()

    request.addfinalizer(teardown)
    return session


def test_creation(session, fs):
    assert fs.Exists("/.trash/contents.json")


def test_print(session, capsys):
    session._print("out")
    session._print("err", use_stderr=True)
    out, err = capsys.readouterr()
    assert out == "out\n"
    assert err == "err\n"


def test_inform(session, capsys):
    session._inform("x")
    out, _ = capsys.readouterr()
    assert out == ""
    session.verbose = True
    session._inform("x")
    out, _ = capsys.readouterr()
    assert out == "x\n"
    session.silent = True
    session._inform("x")
    out, _ = capsys.readouterr()
    assert out == ""


def test_config(fs):
    fs.CreateFile(
        "/.rmt", contents="[config]\n"
                          "remove_interactive: always\n"
                          "[end]\n"
                          "[trashcan]\n"
                          "/dir\n"
                          "[end]"
    )
    session = rmt.core.TrashcanSession(
        config_path="/.rmt")
    assert session.remove_interactive == "always"
    assert fs.Exists("/dir/contents.json")
    session.close()


def test_config_nonexistent(fs):
    with pytest.raises(IOError):
        session = rmt.core.TrashcanSession(
            trashcan_path="/.trash", config_path="/.rmt")
        session.close()


def test_config_bad(fs, capsys):
    fs.CreateFile("/.rmt", contents="[aaa]")
    with pytest.raises(ValueError):
        rmt.core.TrashcanSession(
            trashcan_path="/.trash", config_path="/.rmt")
    _, err = capsys.readouterr()
    assert err == "can`t identify a line\n"


def test_remove(session, fs):
    fs.CreateFile("/file")
    result = session.remove("/file")
    assert result is True
    assert not fs.Exists("/file")
    assert fs.Exists("/.trash/1")
    fs.CreateDirectory("/dir")
    result = session.remove("/dir")
    assert result is True
    assert not fs.Exists("/dir")
    assert fs.Exists("/.trash/2")


def test_remove_nonexistent(session, capsys):
    result = session.remove("/file")
    assert result is False
    _, err = capsys.readouterr()
    assert err == "cannot remove '/file': No such file or directory\n"


def test_remove_nonexistent_force(session, capsys):
    session.remove_interactive = "force"
    result = session.remove("/file")
    _, err = capsys.readouterr()
    assert err == ""
    assert result is True


def test_remove_write_protected(session, fs, capsys, emu_in):
    fs.CreateFile("/file")
    os.chmod("/file", stat.S_IREAD)
    emu_in.send("n")
    session.remove("/file")
    _, err = capsys.readouterr()
    assert err == "remove write-protected file '/file'? "
    assert fs.Exists("/file")
    fs.CreateDirectory("/dir")
    os.chmod("/dir", stat.S_IREAD)
    emu_in.send("n")
    session.remove("/dir")
    _, err = capsys.readouterr()
    assert err == "remove write-protected directory '/dir'? "
    assert fs.Exists("/dir")


def test_remove_interactive_always(session, fs, capsys, emu_in):
    session.remove_interactive = "always"
    fs.CreateFile("/file")
    emu_in.send("n")
    result = session.remove("/file")
    _, err = capsys.readouterr()
    assert err == "remove file '/file'? "
    assert result is True
    assert fs.Exists("/file")
    fs.CreateDirectory("/dir")
    emu_in.send("n")
    result = session.remove("/dir")
    _, err = capsys.readouterr()
    assert err == "remove directory '/dir'? "
    assert result is True
    assert fs.Exists("/dir")


def test_remove_root(session, capsys):
    result = session.remove("/")
    _, err = capsys.readouterr()
    assert err == ("it is dangerous to operate on '/'\n"
                   "use no-preserve-root to override this failsafe\n")
    assert result is False


def test_remove_dot(session, capsys):
    result = session.remove(".")
    _, err = capsys.readouterr()
    assert err == "refusing to remove '.' or '..' directory: skipping '.'\n"
    assert result is False


def test_remove_trash(session, fs, capsys):
    result = session.remove("/.trash")
    assert result is False
    _, err = capsys.readouterr()
    assert err == ("cannot remove '/.trash': Cannot move a directory "
                   "'/.trash' into itself '/.trash/1/.trash'.\n")
    assert fs.Exists("/.trash")


def test_remove_regex(session, fs):
    fs.CreateDirectory("/dir")
    fs.CreateFile("/dir/file1")
    fs.CreateFile("/dir/_file1")
    fs.CreateFile("/dir/some")
    result = session.remove_regex("/dir", r"^file")
    assert result is True
    assert not fs.Exists("/dir/file1")
    assert fs.Exists("/dir/_file1")
    assert fs.Exists("/dir/some")


def test_remove_regex_bad(session, fs, capsys):
    fs.CreateDirectory("/dir")
    result = session.remove_regex("/dir", r"*")
    assert result is False
    _, err = capsys.readouterr()
    assert err == "bad regex: nothing to repeat\n"


def test_view_silent(session, fs, capsys):
    fs.CreateFile("/file")
    session.remove("/file")
    session.silent = True
    session.view()
    out, err = capsys.readouterr()
    assert out == ""
    assert err == ""


def test_view(session, fs, capsys):
    fs.CreateFile("/file")
    session.remove("/file")
    session.view()
    out, _ = capsys.readouterr()
    assert out == "Trashcan /.trash\n1 items:\n1: /file\n"


def test_erase(session, fs):
    fs.CreateFile("/file")
    session.remove("/file")
    result = session.erase("/file")
    assert result is True
    assert not fs.Exists("/file")
    assert not fs.Exists("/.trash/1")


def test_erase_interactive_always(session, fs, capsys, emu_in):
    fs.CreateFile("/file")
    session.remove("/file")
    session.erase_interactive = "always"
    emu_in.send("n")
    result = session.erase("/file")
    _, err = capsys.readouterr()
    assert result is True
    assert err == "1: /file\nerase record? "
    assert not fs.Exists("/file")
    assert fs.Exists("/.trash/1")


def test_erase_by_name(session, fs):
    fs.CreateFile("/file")
    fs.CreateFile("/dir/file")
    session.remove("/file", "/dir/file")
    session.erase_by = "name"
    result = session.erase("file")
    assert result is True
    assert session.can.count() == 0


def test_erase_by_id(session, fs):
    fs.CreateFile("/file")
    session.remove("/file")
    fs.CreateFile("/file")
    session.remove("/file")
    session.erase_by = "id"
    result = session.erase(1)
    assert result is True
    assert session.can.count() == 1
    session.can.get(2)


def test_erase_by_politic(session, fs):
    fs.CreateFile("/file")
    fs.CreateFile("/dir/some")
    session.remove("/file", "/dir/some")
    session.erase_by = "politic"
    result = session.erase("all")
    assert result is True
    assert session.can.count() == 0


def test_erase_by_regex(session, fs):
    fs.CreateFile("/file")
    fs.CreateFile("/_file")
    fs.CreateFile("/some")
    session.remove("/file", "/_file", "/some")
    session.erase_by = "regex"
    result = session.erase(r"^file")
    assert result is True
    assert session.can.count() == 2
    session.can.get(2)
    session.can.get(3)


def test_restore(session, fs):
    fs.CreateFile("/file")
    session.remove("/file")
    result = session.restore("/file")
    assert result is True
    assert fs.Exists("/file")
    assert not fs.Exists("/.trash/1")


def test_restore_interactive_always(session, fs, capsys, emu_in):
    fs.CreateFile("/file")
    session.remove("/file")
    session.restore_interactive = "always"
    emu_in.send("n")
    result = session.restore("/file")
    _, err = capsys.readouterr()
    assert result is True
    assert err == "1: /file\nrestore record? "
    assert not fs.Exists("/file")
    assert fs.Exists("/.trash/1")


def test_restore_existent(session, fs, capsys):
    fs.CreateFile("/file")
    session.remove("/file")
    fs.CreateFile("/file")
    result = session.restore("/file")
    assert result is False
    _, err = capsys.readouterr()
    assert err == "cannot restore record 1: '/file' already exists\n"
