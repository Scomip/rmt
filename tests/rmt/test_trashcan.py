import pytest

import rmt.trashcan


@pytest.fixture
def can(request, fs):
    return rmt.trashcan.Trashcan("/.trash")


def test_calc_dir_size(fs):
    fs.CreateDirectory("/dir")
    fs.CreateFile("/dir/file1", st_size=20)
    fs.CreateFile("/dir/file2", st_size=22)
    assert rmt.trashcan._calc_dir_size("/dir") == 42


def test_creation(can, fs):
    assert fs.Exists("/.trash/contents.json")


def test_put_file(can, fs):
    fs.CreateFile("/file")
    can.put("/file")
    assert not fs.Exists("/file")
    assert fs.Exists("/.trash/1")
    assert can.count() == 1
    record = can.get(1)
    assert record.path == "/file"
    assert record.is_dir is False


def test_put_dir(can, fs):
    fs.CreateDirectory("/dir")
    fs.CreateFile("/dir/file1")
    fs.CreateFile("/dir/file2")
    can.put("/dir")
    assert not fs.Exists("/dir")
    assert fs.Exists("/.trash/1")
    assert can.count() == 1
    record = can.get(1)
    assert record.path == "/dir"
    assert record.is_dir is True


def test_put_nonexistent(can, fs):
    with pytest.raises(OSError):
        can.put("/file")


def test_erase_bad_argument(can, fs):
    with pytest.raises(TypeError):
        can.erase("string")


def test_erase_file(can, fs):
    fs.CreateFile("/file")
    can.put("/file")
    can.erase(1)
    assert not fs.Exists("/file")
    assert not fs.Exists("/.trash/1")
    assert can.count() == 0


def test_erase_dir(can, fs):
    fs.CreateDirectory("/dir")
    fs.CreateFile("/dir/file")
    can.put("/dir")
    can.erase(1)
    assert not fs.Exists("/dir")
    assert not fs.Exists("/.trash/1")
    assert can.count() == 0


def test_restore_bad_argument(can, fs):
    with pytest.raises(TypeError):
        can.restore("string")


def test_restore(can, fs):
    fs.CreateFile("/file")
    can.put("/file")
    can.restore(1)
    assert fs.Exists("/file")
    assert not fs.Exists("/.trash/1")
    assert can.count() == 0


def test_items(can, fs):
    fs.CreateFile("/file1")
    fs.CreateFile("/file2")
    can.put("/file1")
    can.put("/file2")
    items_list = list(can.items())
    assert items_list[0].record_id == 1
    assert items_list[1].record_id == 2


def test_reopen(fs):
    can = rmt.trashcan.Trashcan("/.trash")
    fs.CreateFile("/file")
    can.put("/file")
    can.save()
    del can
    can = rmt.trashcan.Trashcan("/.trash")
    assert can.count() == 1
    record = can.get(1)
    assert record.path == "/file"


def test_KeyError(can):
    with pytest.raises(rmt.trashcan.RecordNotFoundError) as exc_info:
        can.erase(42)
    assert exc_info.value.message == "record 42 not found"
    with pytest.raises(rmt.trashcan.RecordNotFoundError) as exc_info:
        can.restore(42)
    assert exc_info.value.message == "record 42 not found"
