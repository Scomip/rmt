"""Core module of rmt.

Holds TrashcanSession class.

"""
import datetime
import logging
import os
import re
import sys

from collections import defaultdict

import rmt.config
import rmt.trashcan

LOGGER = logging.getLogger(__name__)


class TrashcanSession(object):
    """rmt.Trashcan wrapper.

    TrashcanSession wraps rmt.Trashcan and allows user to work with
    trashcan in interactive manner.

    TrashcanSession holds a config:
        silent - If True, session won`t produce any output.
        verbose - If True, session will produce additional output.
        dry_run - If True, session wont do any real operations.
        record_format - Format string, used to display a single record.
            {n} - Id.
            {path} - Path to the file or directory.
            {name} - File or directory name.
            {dtime} - Deletion time.
            {mtime} - Modification time.
            {size} - Size.
            {d} - Directory flag.
            {l} - Link flag.
        remove_interacitve - Interactive mode for remove operations.
            Can be 'force', 'never' or 'always'.
        preserve_root - If True, session won`t run remove on root.
        erase_interactive - Interactive mode for erase operations. Can
            be 'force', 'never' or 'always'.
        erase_by - What will be used to choose records to erase. Can
            be 'path', 'name', 'id', 'politic' or 'regex'.
        restore_interactive - Interactive mode for restore operations.
            Can be 'force', 'never' or 'always'.
        restore_by - What will be used to choose records to restore.
            Can be 'path', 'name', 'id', 'politic' or 'regex'.
        view_header_format - Format string for view header.
            {n} - Number of files in trashcan.
            {path} - Path to the trashcan.

    Config parameters load from config file.
    Config parameters can be passed into __init__(), so they will
    override parameters, loaded from config.

    Operations:
        remove - Remove a list of files or directories.
        remove_regex - Remove files from subtree by regex.
        erase - Erase records.
        restore - Restore records.
        view - View trashcan contents.

    """
    DEFAULT_CONFIG_PATH = u"~/.rmt"
    DEFAULT_TRASHCAN_PATH = u"~/.trash"

    class _OperationError(RuntimeError):
        pass

    class _OperationIgnored(Exception):
        pass

    def __init__(self, config_path=None, trashcan_path=None, **kwargs):
        rmt.config.validate_config(kwargs)

        self.silent = False
        self.verbose = False
        self.dry_run = False
        self.record_format = "{n}: {path}"
        self.remove_interactive = "never"
        self.preserve_root = True
        self.erase_interactive = "never"
        self.erase_by = "path"
        self.restore_interactive = "never"
        self.restore_by = "path"
        self.view_header_format = "Trashcan {path}\n{n} items:"

        self.politics = {"all": []}

        self.trashcan_path = os.path.expanduser(self.DEFAULT_TRASHCAN_PATH)
        self.config_path = os.path.expanduser(self.DEFAULT_CONFIG_PATH)
        self._load_config(config_path)
        self.__dict__.update(kwargs)
        self._load_trashcan(trashcan_path)

        LOGGER.info("Trashcan session opened")

    def remove(self, *paths):
        """Remove files by putting them into trashcan.

        Will prompt before every operation if 'remove_interactive'
        config option is 'always'. Will inform when file does not
        exists unless `remove_interactive` config opetion is 'force'.
        Use 'preserve_root' to preserve '/' directory from deletion
        (default is True).

        Args:
            *paths: The paths to files and directories to be removed.

        Returns:
            The return value. True if success, False otherwise.

        """
        result, removed_cnt = self._perform_for_all(paths, self._remove_entry)
        self._inform("{} entries removed".format(removed_cnt))
        return result

    def remove_regex(self, directory, regex_str):
        """Remove all files from directory subtree, that match regex.

        Will prompt before every operation if 'remove_interactive'
        config option is 'always'.
        Use 'preserve_root' to preserve '/' directory from deletion
        (default is True).

        Args:
            directory (str): A subtree to remove files from.
            regex (str, must be a a valid regex): A regular expression
            used for matching.

        Returns:
            The return value. True if success, False otherwise.

        """
        ex_directory = os.path.expanduser(directory)
        LOGGER.info("removing in %s by regex %s", ex_directory, regex_str)
        if not os.path.isdir(ex_directory):
            if self.remove_interactive != "force":
                self._print("directory {} not found".format(ex_directory),
                            use_stderr=True)
                return False
            return True
        try:
            regex = re.compile(regex_str)
        except re.error as exc:
            self._print("bad regex: {}".format(exc.message), use_stderr=True)
            return False
        result = True
        removed_cnt = 0
        for dir_path, _, files in os.walk(ex_directory):
            def _remove_matching(file_name, dir_path=dir_path):
                if regex.match(file_name):
                    file_path = os.path.join(dir_path, file_name)
                    self._remove_file(file_path)
            part_result, part_cnt = self._perform_for_all(
                files, _remove_matching)
            result &= part_result
            removed_cnt += part_cnt
        self._inform("{} files removed".format(removed_cnt))
        return result

    def erase(self, *targets):
        """Erase target records from the trashcan.

        Use 'erase_by' config option to choose what type of target
        need to be used:
            'path' - erase records with the target path.
            'name' - erase records with the target file name.
            'id' - erase a record with the target id.
            'politic' - choose records with the target politic.
            'regex' - erase records with the file name, that matches
            target regex.
        Will prompt before every erase if 'erase_interactive' config
        is 'always'. Will inform when target does not find any record
        unless 'erase_interactive' config option is 'force'.
        Uses 'record_format' to display trashcan records.

        Args:
            *targets: Targets used to chooose records for erase.

        Returns:
            The return value. True if success, False otherwise.

        """
        result = True
        erased_cnt = 0
        for target in targets:
            try:
                records = self._get_filtered_records(target, self.erase_by)
            except RuntimeError as exc:
                self._print(exc.message)
                result = False
                continue
            if not records:
                if self.erase_interactive != "force":
                    self._print("no records found for {}".format(target),
                                use_stderr=True)
                    result = False
                continue
            part_result, part_cnt = self._perform_for_all(
                records, self._erase_record)
            result &= part_result
            erased_cnt += part_cnt
        self._inform("{} records erased".format(erased_cnt))
        return result

    def restore(self, *targets):
        """Restore targets from the trashcan.

        Use 'restore_by' config option to choose what type of target
        need to be used:
            'path' - restore records with the target path.
            'name' - restore records with the target file name.
            'id' - restore a record with the target id.
            'politic' - choose records with the target politic.
            'regex' - restore records with the file name, that matches
            target regex.
        Will prompt before every restore if 'restore_interactive' config
        is 'always'. Will inform when target does not find any record
        unless 'restore_interactive' config option is 'force'.
        Uses 'record_format' to display trashcan records.

        Args:
            *targets: Targets used to chooose records for erase.

        Returns:
            The return value. True if success, False otherwise.

        """
        result = True
        restored_cnt = 0
        for target in targets:
            try:
                records = self._get_filtered_records(target, self.restore_by)
            except RuntimeError as exc:
                self._print(exc.message)
                result = False
                continue
            if not records:
                if self.erase_interactive != "force":
                    self._print("no records found for '{}'".format(target),
                                use_stderr=True)
                    result = False
                continue
            records_by_path = defaultdict(list)
            for record in records:
                records_by_path[record.path].append(record)
            part_result, part_cnt = self._perform_for_all(
                records_by_path.iteritems(), self._restore_by_path)
            result &= part_result
            restored_cnt += part_cnt
        self._inform("{} files restored".format(restored_cnt))
        return result

    def view(self):
        """View trashcan contents.

        Uses 'view_header_format' config option to display view header.
        Uses 'record_format' config option to display single trashcan
        record.

        """
        if self.silent:
            return
        print self.view_header_format.format(
            path=self.can.path,
            n=self.can.count()
        )
        for item in self.can.items():
            print self._view_record(item)

    def close(self):
        """Finish the session and save the trashcan contents."""
        if hasattr(self, "can"):
            self.can.save()
        LOGGER.info("Trashcan session closed")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.close()

    def _load_config(self, config_path):
        using_default = True
        if config_path is not None:
            self.config_path = os.path.expanduser(config_path)
            using_default = False
        try:
            LOGGER.info("loading config from %s", self.config_path)
            with open(self.config_path) as config_file:
                loaded_dict = rmt.config.parse_custom_config(config_file)
            if loaded_dict["trashcan"] is not None:
                self.trashcan_path = loaded_dict["trashcan"]
            self.__dict__.update(loaded_dict["config"])
            self.politics.update(loaded_dict["politics"])
        except IOError:
            if using_default:
                LOGGER.warning("failed to load config from default path")
                self.config_path = None
            else:
                LOGGER.critical("failed to load config")
                raise
        except ValueError as exc:
            self._print(exc.message, use_stderr=True)
            LOGGER.critical("failed to load config")
            raise

    def _load_trashcan(self, trashcan_path):
        if trashcan_path is not None:
            self.trashcan_path = os.path.expanduser(trashcan_path)
        LOGGER.info("loading trashcan from %s", self.trashcan_path)
        self.can = rmt.trashcan.Trashcan(self.trashcan_path)

    def _remove_entry(self, path):
        ex_path = os.path.expanduser(path)
        if not os.path.exists(ex_path):
            LOGGER.error("%s can`t be removed: not found", ex_path)
            if self.remove_interactive == "force":
                raise self._OperationIgnored
            else:
                msg = ("cannot remove '{}': No such file or directory"
                       .format(ex_path))
                raise self._OperationError(msg)
        if os.path.isdir(ex_path):
            self._remove_dir(ex_path)
        else:
            self._remove_file(ex_path)

    def _remove_file(self, path):
        if self.remove_interactive != "force":
            if not os.access(path, os.W_OK):
                if not self._ask("remove write-protected file '{}'?"
                                 .format(path)):
                    raise self._OperationIgnored
            elif self.remove_interactive == "always":
                if not self._ask("remove file '{}'?".format(path)):
                    raise self._OperationIgnored
        self._remove_path(path)

    def _remove_dir(self, path):
        if path == '/' and self.preserve_root:
            msg = ("it is dangerous to operate on '/'\n"
                   "use no-preserve-root to override this failsafe")
            raise self._OperationError(msg)
        basename = os.path.basename(path)
        if basename == '.' or basename == '..':
            msg = ("refusing to remove '.' or '..' directory: "
                   "skipping '{}'".format(path))
            raise self._OperationError(msg)
        if not os.access(path, os.W_OK):
            if not self._ask("remove write-protected directory '{}'?"
                             .format(path)):
                raise self._OperationIgnored
        elif self.remove_interactive == "always":
            if not self._ask("remove directory '{}'?".format(path)):
                raise self._OperationIgnored
        self._remove_path(path)

    def _remove_path(self, path):
        LOGGER.info("removing %s", path)
        if not self.dry_run:
            try:
                self.can.put(path)
            except rmt.trashcan.NotEnoughSpaceError:
                msg = "cannot remove '{}': not enough space".format(path)
                raise self._OperationError(msg)
            except (OSError, RuntimeError) as exc:
                raise self._OperationError("cannot remove '{}': {}"
                                           .format(path, exc.message))
        self._inform("removed {}".format(path))

    def _perform_for_all(self, target_list, operation):
        result = True
        counter = 0
        for target in target_list:
            try:
                operation(target)
                counter += 1
            except self._OperationError as exc:
                self._print(exc.message, use_stderr=True)
                result = False
            except self._OperationIgnored:
                pass
        return result, counter

    def _print(self, msg, use_stderr=False):
        if not self.silent:
            if use_stderr:
                print >> sys.stderr, msg
            else:
                print msg

    def _inform(self, msg):
        if self.verbose and not self.silent:
            print msg

    def _inform_record(self, record):
        if self.verbose and not self.silent:
            print self._view_record(record)

    def _ask(self, msg):
        if not self.silent:
            sys.stderr.write(msg + " ")
        responce = raw_input()
        result = responce[0] == "y" or responce[0] == "Y"
        return result

    def _erase_record(self, record):
        LOGGER.info("erasing record %i", record.record_id)
        if self.erase_interactive == "always":
            self._print(self._view_record(record), use_stderr=True)
            if not self._ask("erase record?"):
                raise self._OperationIgnored
        try:
            if not self.dry_run:
                self.can.erase(record.record_id)
        except (OSError, RuntimeError) as exc:
            raise self._OperationError("cannot erase record {}: {}"
                                       .format(record.record_id, exc.message))
        self._inform("erased record:")
        self._inform_record(record)

    def _restore_by_path(self, target):
        path, records = target
        records_cnt = len(records)
        if records_cnt > 1:
            self._print("{} records can be restored for {}"
                        .format(records_cnt, path), use_stderr=True)
            for record in records:
                self._print(self._view_record(record), use_stderr=True)
            self._print("ignoring, restore by id instead", use_stderr=True)
            raise self._OperationIgnored
        self._restore_record(records[0])

    def _restore_record(self, record):
        LOGGER.info("restoring record %i", record.record_id)
        if self.restore_interactive == "always":
            self._print(self._view_record(record), use_stderr=True)
            if not self._ask("restore record?"):
                raise self._OperationIgnored
        try:
            if not self.dry_run:
                self.can.restore(record.record_id)
        except rmt.trashcan.NotEnoughSpaceError:
            msg = "cannot restore '{}': not enough space".format(record.path)
            raise self._OperationError(msg)
        except (OSError, RuntimeError) as exc:
            raise self._OperationError(
                "cannot restore record {}: {}"
                .format(record.record_id, exc.message))
        self._inform("restored: {}".format(record.path))
        self._inform("from record:")
        self._inform_record(record)

    def _view_record(self, record):
        return self.record_format.format(
            n=record.record_id,
            path=record.path,
            name=os.path.basename(record.path),
            mtime=datetime.datetime.fromtimestamp(record.mtime),
            dtime=datetime.datetime.fromtimestamp(record.dtime),
            size=record.size,
            d=("D" if record.is_dir else "F"),
            l=("L" if record.is_link else " ")
        )

    def _get_filtered_records(self, value, filter_by):
        if filter_by == "path":
            target_path = os.path.abspath(os.path.expanduser(value))

            def _filter_func(record):
                return record.path == target_path

        elif filter_by == "name":

            def _filter_func(record):
                return os.path.basename(record.path) == value

        elif filter_by == "id":
            target_id = int(value)

            def _filter_func(record):
                return record.record_id == target_id

        elif filter_by == "politic":
            if value not in self.politics:
                raise RuntimeError("politic {} not found".format(value))
            politic = self.politics[value]
            if not politic:

                def _filter_func(_):
                    return True
            else:
                current_time = datetime.datetime.now()

                def _filter_func(record):
                    for cond in politic:
                        dtime = datetime.datetime.fromtimestamp(record.dtime)
                        if (current_time - dtime > cond[0] and
                                record.size > cond[1]):
                            return True
                        return False

        elif filter_by == "regex":
            try:
                regex = re.compile(value)
            except re.error as exc:
                raise RuntimeError("bad regex: {}".format(exc.message))

            def _filter_func(record):
                return bool(regex.match(os.path.basename(record.path)))

        return [record for record in self.can.items() if _filter_func(record)]
