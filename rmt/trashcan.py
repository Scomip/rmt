"""Trashcan module of rmt.

Holds Trashcan class.

"""
import json
import logging
import os
import os.path
import shutil
import time

from collections import namedtuple

LOGGER = logging.getLogger(__name__)


class RecordNotFoundError(RuntimeError):
    """Raised when target record does not exists."""
    def __init__(self, id_):
        super(RecordNotFoundError, self).__init__()
        self.message = "record {} not found".format(id_)


class NotEnoughSpaceError(RuntimeError):
    """Raised when free space is not enough to finish the operation."""
    pass


TrashRecord = namedtuple(
    "TrashRecord",
    ["record_id", "path", "mtime", "dtime",
     "size", "is_dir", "is_link"]
)


class Trashcan(object):
    """Set of files in a certain directory.

    Methods:
        put - Put a file or directory into trashcan.
        erase - Erase a record and remove associated file or
            directory.
        restore - Erase a record and restore associated file or
            directory and restore to its real location.
        save - Save information about Trashcan on drive.
        items - Generator on Trashcan contents.
        count - Number of records.
        get - Get a record from id.

    """
    def __init__(self, path):
        abs_path = os.path.abspath(os.path.expanduser(path))
        self.path = abs_path
        contents_path = os.path.join(self.path, "contents.json")
        try:
            LOGGER.info("loading contents from %s", self.path)
            with open(contents_path) as contents_file:
                dct = json.load(contents_file)
                self.next_id = dct["next_id"]
                records = dct["records"]
                self.records = {
                    int(record_id): TrashRecord(*records[record_id])
                    for record_id in records
                }
                LOGGER.info("trashcan contents loaded at %s", self.path)
        except IOError:
            LOGGER.warning("failed to load %s", contents_path)
            self._create_new(self.path)
        self.dev = os.stat(self.path).st_dev

    def put(self, path):
        """Put a file into the trashcan.

        Args:
            path: Path to a file or directory that needs to be put.

        Raises:
            RuntimeError, OSError, NotEnoughSpaceError.

        """
        LOGGER.info("putting %s into trashcan (%s)", path, self.path)
        record_id = self.next_id
        self.next_id += 1
        record = self._put_to_trashcan(path, self.path, record_id)
        self.records[record_id] = record
        return record

    def erase(self, target_id):
        """Erase a file and the record with the target id.

        Args:
            target_id: Id of a record to erase.

        Raises:
            TypeError, RecordNotFoundError, RuntimeError, OSError.

        """
        if not isinstance(target_id, int):
            raise TypeError("erase expected int")
        LOGGER.info("erasing record %i from trashcan (%s)", target_id,
                    self.path)
        if target_id not in self.records:
            raise RecordNotFoundError(target_id)
        self._erase_from_trashcan(self.path, target_id)
        return self._remove_record(target_id)

    def restore(self, target_id):
        """Restore a file from the record with the target id.

        Args:
            target_id: Id of a record to restore.

        Raises:
            TypeError, RecordNotFoundError, RuntimeError, OSError,
            NotEnoughSizeError.

        """
        if not isinstance(target_id, int):
            raise TypeError("restore expected int")
        LOGGER.info("restoring record %i from trashcan (%s)",
                    target_id, self.path)
        try:
            record = self.records[target_id]
        except KeyError:
            raise RecordNotFoundError(target_id)
        self._restore_from_trashcan(self.path, target_id, record.path, record.size)
        return self._remove_record(target_id)

    def save(self):
        """Save trashcan contents."""
        contents_path = os.path.join(self.path, "contents.json")
        LOGGER.info("saving trashcan contents at %s", contents_path)
        try:
            os.makedirs(self.path)
        except OSError as exc:
            if exc.errno != os.errno.EEXIST:
                raise
        with open(contents_path, "w") as contents_file:
            json.dump(
                {"next_id": self.next_id, "records": self.records},
                contents_file, indent=4
            )

    def items(self):
        """Iterate on all trashcan record, sorted by id."""
        for (_, record) in sorted(self.records.items()):
            yield record

    def count(self):
        """Return a number of records."""
        return len(self.records)

    def get(self, record_id):
        """Return a record with the target id."""
        return self.records[record_id]

    @staticmethod
    def _restore_from_trashcan(can_path, record_id, restore_path, size):
        content_path = os.path.join(can_path, str(record_id))
        dir_path = os.path.dirname(restore_path)
        try:
            os.makedirs(dir_path)
        except OSError as exc:
            if exc.errno != os.errno.EEXIST:
                raise
        can_dev = os.stat(can_path).st_dev
        dest_dev = os.stat(dir_path).st_dev
        _move(content_path, restore_path, size, can_dev, dest_dev)

    @staticmethod
    def _put_to_trashcan(path, can_path, record_id):
        abs_path = os.path.abspath(os.path.expanduser(path))

        target_stat = os.stat(abs_path)
        file_dev = target_stat.st_dev
        can_dev = os.stat(can_path).st_dev

        is_dir = os.path.isdir(abs_path)
        is_link = os.path.islink(abs_path)

        record = TrashRecord(
            record_id=record_id,
            path=abs_path,
            mtime=target_stat.st_mtime,
            dtime=time.time(),
            size=(_calc_dir_size(abs_path)
                  if is_dir and not is_link
                  else target_stat.st_size),
            is_dir=os.path.isdir(abs_path),
            is_link=os.path.islink(abs_path),
        )

        content_path = os.path.join(can_path, str(record_id))
        _move(abs_path, content_path, record.size, file_dev, can_dev)
        return record

    @staticmethod
    def _erase_from_trashcan(can_path, record_id):
        path = os.path.join(can_path, str(record_id))
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)

    def _create_new(self, path):
        LOGGER.info("creating new trashcan at %s", path)
        self.next_id = 1
        self.path = path
        self.records = {}
        self.save()

    def _remove_record(self, record_id):
        LOGGER.debug("removing record %i from trashcan (%s)",
                     record_id, self.path)
        return self.records.pop(record_id)


def _move(src, dest, size, src_dev, dest_dev):
    dest_dir = os.path.dirname(dest)
    if src_dev != dest_dev:
        fs_stat = os.statvfs(dest_dir)
        free_space = fs_stat.f_bsize * fs_stat.f_bavail
        if free_space < size:
            raise NotEnoughSpaceError
    if os.path.exists(dest):
        raise RuntimeError("'{}' already exists".format(dest))
    try:
        shutil.move(src, dest)
    except shutil.Error as exc:
        raise RuntimeError(exc.message)


def _calc_dir_size(path):
    size = 0
    for dir_path, _, files in os.walk(path):
        for file_name in files:
            file_path = os.path.join(dir_path, file_name)
            size += os.path.getsize(file_path)
    return size
