"""rmt package.

Classes:
    Trashcan - stores files in a certain directory and allows to
    erase and restore them.
    TrashcanSession - Trashcan wrapper for interactive work.

"""
import logging

from rmt.core import TrashcanSession
from rmt.trashcan import Trashcan

logging.getLogger(__name__).addHandler(logging.NullHandler())
