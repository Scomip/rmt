"""Config module of rmt.

Holds constants and functions to work with TrashcanSession config.

Functions:
    validate_config - Check config validity.
    translate - Translate config option.

    parse_custom_config
    parse_trashcan_section
    parse_config_section
    parse_politics_section
"""
import re
import datetime

SIZES = {
    "b": 1,
    "kb": 1024,
    "mb": 1024**2,
    "gb": 1024**3
}
IGNORED_LINE_REGEX = re.compile(
    r"^\s*(?:#.*)?$")
SECTION_REGEX = re.compile(
    r"^\[(trashcan|config|politics)]$")
END_SECTION_REGEX = re.compile(
    r"\[end]")
POLITIC_NAME_REGEX = re.compile(
    r"^(\w+):$")
POLITIC_CONDITION_REGEX = re.compile(
    r"^(\d+)-(\d+):(\d+):(\d+)\s+(\d+)(b|kb|mb|gb|tb)$")
CONFIG_REGEX = re.compile(
    r"^([a-z_]+):\s+(\w+)$")
CONFIG_CHOICES = {
    "silent": bool,
    "verbose": bool,
    "dry_run": bool,
    "record_format": basestring,
    "remove_interactive": ("never", "always", "force"),
    "preserve_root": bool,
    "erase_interactive": ("never", "always", "force"),
    "erase_by": ("path", "name", "id", "politic", "regex"),
    "restore_interactive": ("never", "always", "force"),
    "restore_by": ("path", "name", "id", "politic", "regex"),
    "view_header_format": basestring,
}
RESERVED_POLITICS = ["all"]


def validate_config(config):
    """Check if dict can be a valid TrashcanSession config.

    Raises:
        ValueError and TypeError.

    """
    for arg in config.iterkeys():
        if arg not in CONFIG_CHOICES:
            raise ValueError("unexpected key: {}".format(arg))
        if isinstance(CONFIG_CHOICES[arg], tuple):
            if not config[arg] in CONFIG_CHOICES[arg]:
                raise ValueError("value for {} is not in choices".format(
                    arg))
        else:
            if not isinstance(config[arg], CONFIG_CHOICES[arg]):
                raise TypeError("{} expected for {}, found {}".format(
                    repr(CONFIG_CHOICES[arg]), arg,
                    repr(type(config[arg]))))


def translate(value, choices):
    """Translate value into config option.

    Args:
        value: Value to be translated.
        choices: Either type or tuple of possible choices.

    Returns:
        Translated value.

    Raises:
        ValueError.

    """
    if isinstance(choices, tuple):
        if value in choices:
            return value
        else:
            raise ValueError("invalid value: {}".format(value))
    elif choices == bool:
        if value == "true":
            return True
        elif value == "false":
            return False
        else:
            raise ValueError("invalid value: {}".format(value))
    elif choices == basestring:
        return value
    else:
        raise ValueError("invalid value: {}".format(value))


def parse_trashcan_section(lines):
    """Parse trashcan section from config.

    Valid trashcan section starts with '[trashcan]' and ends
    with '[end]' and contains exactly one line with the path
    to trashcan directory.

    Args:
        lines: Generator, that returns lines of config.

    Returns:
        Parsed path to trashcan or None.

    Raises:
        ValueError.

    """
    path = None
    for line in lines:
        if END_SECTION_REGEX.match(line):
            return path
        if path is None:
            path = line.strip("\n")
        else:
            raise ValueError("trashcan section must contain at most one line")
    raise ValueError("expecting end of section")


def parse_config_section(lines):
    """Parse config section from config.

    Valid config section starts with '[config]' and ends
    with '[end]' and contains multiple lines in a form of
    'key: value', where all keys are different string from
    CONFIG_CHOICES and value is valid value for that key.

    Args:
        lines: Generator, that returns lines of config.

    Returns:
        Parsed and translated config dict.

    Raises:
        ValueError.

    """
    config = {}
    for line in lines:
        if IGNORED_LINE_REGEX.match(line):
            continue
        if END_SECTION_REGEX.match(line):
            return config
        match = CONFIG_REGEX.match(line.strip())
        if not match:
            raise ValueError("can`t identify a line")
        key = match.group(1)
        if key in config:
            raise ValueError("config key '{}' already met".format(key))
        config[key] = translate(match.group(2), CONFIG_CHOICES[key])
    raise ValueError("expecting end of section")


def parse_politics_section(lines):
    """Parse politics section from config.

    Valid politics section starts with '[politics]' and ends
    with '[end]' and contains multiple politics in a form of
    'name:
        condition
        condition
        ...', where all names are different and conditions have
    a form of '<days>-<hours>:<minutes>:<seconds> <size><b|kb|mb|gb>'.

    Args:
        lines: Generator, that returns lines of config.

    Returns:
        Parsed and translated politics dict.

    Raises:
        ValueError.

    """
    politics = {}
    current_politic = None
    for line in lines:
        if IGNORED_LINE_REGEX.match(line):
            continue
        if current_politic:
            match = POLITIC_CONDITION_REGEX.match(line.strip())
            if match:
                groups = match.groups()
                [days, hours, minutes, seconds, size] = [
                    int(value) for value in groups[:-1]]
                size_mul = groups[-1]
                politic = (
                    datetime.timedelta(
                        days=days,
                        hours=hours, minutes=minutes, seconds=seconds),
                    size * SIZES[size_mul])
                politics[current_politic].append(politic)
                continue
            else:
                current_politic = None
        if END_SECTION_REGEX.match(line):
            return politics
        match = POLITIC_NAME_REGEX.match(line.strip())
        if not match:
            raise ValueError("can`t identify a line")
        name = match.group(1)
        if name in politics:
            raise ValueError("politic '{}' already met".format(name))
        if name in RESERVED_POLITICS:
            raise ValueError("politic '{}' is reserved".format(name))
        politics[name] = []
        current_politic = name
    raise ValueError("expecting end of section")


SECTION_PARSERS = {
    "trashcan": parse_trashcan_section,
    "config": parse_config_section,
    "politics": parse_politics_section
}


def parse_custom_config(lines):
    """Parse config for TrashcanSession.

    Valid config consists of sections: trashcan, config and politics.
    Each section can appear not more than one time in a config.

    Args:
        lines: Generator, that returns lines of config.

    Raises:
        ValueError.

    Returns:
        A dict of parsed and translated sections.

    """
    result = {"trashcan": None, "config": {}, "politics": {}}
    section_met = {"trashcan": False, "config": False, "politics": False}
    for line in lines:
        if IGNORED_LINE_REGEX.match(line):
            continue
        match = SECTION_REGEX.match(line)
        if not match:
            raise ValueError("can`t identify a line")
        section = match.group(1)
        if section_met[section]:
            raise ValueError("section '{}' already met".format(section))
        section_met[section] = True
        result[section] = SECTION_PARSERS[section](lines)
    return result
