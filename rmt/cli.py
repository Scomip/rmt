"""Cli module of rmt.

Holds parsers of program cli arguments and functions to run a program.

Functions:
    run_rmt - Run a program.
    run_rmtrm - Run a 'rmt remove' shortcut.
    run - Run a program with args.

"""
import argparse
import sys

import rmt.core


def _create_parsers():
    common_parser = argparse.ArgumentParser(
        add_help=False,
        argument_default=argparse.SUPPRESS
    )
    common_parser.add_argument(
        "-c", "--config",
        action="store",
        dest="config_path",
        help="path to a config file"
    )
    common_parser.add_argument(
        "-t", "--trashcan",
        action="store",
        dest="trashcan_path",
        help="path to a trashcan"
    )
    common_parser.add_argument(
        "-s", "--silent",
        action="store_true",
        help="produce no output"
    )
    common_parser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="explain what is being done"
    )
    common_parser.add_argument(
        "--dry-run",
        action="store_true",
        help="don`t perform any real actions"
    )
    common_parser.add_argument(
        "--record-format",
        action="store",
        help="format string to display trash record"
    )
    root_parser = argparse.ArgumentParser(
        prog="rmt",
        argument_default=argparse.SUPPRESS
    )
    subparsers = root_parser.add_subparsers(dest="subcommand")
    basic_remove_parser = argparse.ArgumentParser(
        add_help=False,
        argument_default=argparse.SUPPRESS
    )
    basic_remove_parser.add_argument(
        "-i",
        action="store_const",
        dest="remove_interactive",
        const="always",
        default="never",
        help="prompt before every action"
    )
    basic_remove_parser.add_argument(
        "-f", "--force",
        action="store_const",
        dest="remove_interactive",
        const="force",
        default="never",
        help="ignore nonexistent files and arguments, never prompt"
    )
    basic_remove_parser.add_argument(
        "--interactive",
        action="store",
        dest="remove_interactive",
        choices=["never", "always", "force"],
        default="always",
        metavar="WHEN",
        help="prompt according to WHEN: force (-f), never, always (-i); "
             "without WHEN, prompt always"
    )
    basic_remove_parser.add_argument(
        "--no-preserve-root",
        action="store_false",
        dest="preserve_root",
        help="do not treat '/' specially"
    )
    basic_remove_parser.add_argument(
        "--preserve-root",
        action="store_true",
        dest="preserve_root",
        help="do not remove '/' (default)"
    )
    remove_parser = subparsers.add_parser(
        "remove",
        parents=[common_parser, basic_remove_parser],
        argument_default=argparse.SUPPRESS,
        help="move file(s) to trashcan"
    )
    remove_parser.add_argument(
        "paths",
        nargs="+",
        help="file or directory to remove"
    )
    remove_regex_parser = subparsers.add_parser(
        "remove-regex",
        parents=[common_parser, basic_remove_parser],
        argument_default=argparse.SUPPRESS,
        help="move all files, that satisfy regex, from a subtree"
    )
    remove_regex_parser.add_argument(
        "subtree",
        help="subtree to remove files from"
    )
    remove_regex_parser.add_argument(
        "regex",
        help="regex to use for matching"
    )
    basic_erase_parser = argparse.ArgumentParser(
        add_help=False,
        argument_default=argparse.SUPPRESS
    )
    basic_erase_parser.add_argument(
        "-i",
        action="store_const",
        dest="erase_interactive",
        const="always",
        default="never",
        help="prompt before every action"
    )
    basic_erase_parser.add_argument(
        "-f", "--force",
        action="store_const",
        dest="erase_interactive",
        const="force",
        default="never",
        help="ignore targets that do nothing, never prompt"
    )
    basic_erase_parser.add_argument(
        "--interactive",
        action="store",
        dest="erase_interactive",
        choices=["never", "always", "force"],
        default="always",
        metavar="WHEN",
        help="prompt according to WHEN: force (-f), never, always (-i); "
             "without WHEN, prompt always"
    )
    erase_parser = subparsers.add_parser(
        "erase",
        parents=[common_parser, basic_erase_parser],
        argument_default=argparse.SUPPRESS,
        help="remove file(s) from trashcan"
    )
    erase_parser.add_argument(
        "--by",
        action="store",
        dest="erase_by",
        choices=["name", "path", "id", "regex"],
        metavar="BY",
        default="path",
        help="pick file to be erased BY: name, path, id or regex"
    )
    erase_parser.add_argument(
        "targets",
        nargs="+",
        help="targets, that will be used to find records to erase"
    )
    autoclean_parser = subparsers.add_parser(
        "autoclean",
        parents=[common_parser, basic_erase_parser],
        argument_default=argparse.SUPPRESS,
        help="erase all files that satisfy some politic"
    )
    autoclean_parser.add_argument(
        "politic",
        action="store",
        help="name of a politic to be used"
    )
    restore_parser = subparsers.add_parser(
        "restore",
        parents=[common_parser],
        argument_default=argparse.SUPPRESS,
        help="restore file(s) from trashcan"
    )
    restore_parser.add_argument(
        "-i",
        action="store_const",
        dest="restore_interactive",
        const="always",
        default="never",
        help="prompt before every action"
    )
    restore_parser.add_argument(
        "-f", "--force",
        action="store_const",
        dest="restore_interactive",
        const="force",
        default="never",
        help="ignore targets that do nothing, never prompt"
    )
    restore_parser.add_argument(
        "--interactive",
        action="store",
        dest="restore_interactive",
        choices=["never", "always", "force"],
        default="always",
        metavar="WHEN",
        help="prompt according to WHEN: force (-f), never, always (-i); "
             "without WHEN, prompt always"
    )
    restore_parser.add_argument(
        "--by",
        action="store",
        dest="restore_by",
        choices=["name", "path", "id", "regex"],
        metavar="BY",
        default="path",
        help="pick file to be restored BY: name, path, id or regex"
    )
    restore_parser.add_argument(
        "targets",
        nargs="+",
        help="files, that need to be restored"
    )
    view_parser = subparsers.add_parser(
        "view",
        parents=[common_parser],
        argument_default=argparse.SUPPRESS,
        help="view trashcan content"
    )
    view_parser.add_argument(
        "--view-header-format",
        action="store",
        help="format string to display trashcan header"
    )
    return (root_parser, remove_parser)


def run_rmt():
    """Parse 'rmt' args and run a program.

    Returns:
        0 if success, 1 otherwise.

    """
    parser, _ = _create_parsers()
    args = parser.parse_args()
    return run(args)


def run_rmtrm():
    """Parse 'rmtrm' (an 'rmt remove' shortcut) args and run a program.

    Returns:
        0 if success, 1 otherwise.

    """
    _, parser = _create_parsers()
    args = parser.parse_args()
    args.subcommand = "remove"
    return run(args)


def run(args):
    """Run a program with args.

    Args:
        args (argparse.Namespace): Program args.

    Returns:
        0 if success, 1 otherwise.

    """
    session_config = dict.copy(vars(args))
    del session_config["subcommand"]
    if args.subcommand == "remove":
        del session_config["paths"]
    elif args.subcommand == "remove-regex":
        del session_config["regex"]
        del session_config["subtree"]
    elif args.subcommand == "erase" or args.subcommand == "restore":
        del session_config["targets"]
    elif args.subcommand == "autoclean":
        del session_config["politic"]
        session_config["erase_by"] = "politic"
    try:
        with rmt.core.TrashcanSession(**session_config) as session:
            if args.subcommand == "remove":
                result = session.remove(*args.paths)
            elif args.subcommand == "remove-regex":
                result = session.remove_regex(args.subtree, args.regex)
            elif args.subcommand == "erase":
                result = session.erase(*args.targets)
            elif args.subcommand == "autoclean":
                result = session.erase(args.politic)
            elif args.subcommand == "restore":
                result = session.restore(*args.targets)
            elif args.subcommand == "view":
                session.view()
                result = True
    except RuntimeError as exc:
        print >> sys.stderr, exc.message
        result = False
    return 0 if result else 1
