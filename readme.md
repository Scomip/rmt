# RMT #

rmt is an rm-like command line tool for working with trashcan.

## DESCRIPTION ##

/readme.md - this file  
/setup.py - setuptools script  
/rmt - 'rmt' package  
/rmt/cli.py - command line programs  
/rmt/core.py - module holding TrashcanSession class  
/rmt/trashcan.py - module holding Trashcan class  
/rmt/config.py - module with config utils  
/rmt\_async - 'rmt\_async' package  
/rmt\_async/trashcan.py - module holding AsyncTrashcan class  
/tests - packages tests  
/tests/rmt - rmt package tests  
/tests/rmt\_async - rmt\_async package tests  

## RMT PROGRAM ##
Usage: `rmt <command> [args]`  
- remove - remove files and/or directories  
- remove-regex - remove all files from subtree, thath match regex  
- erase - completely remove entries from trashcan  
- autoclean - erase trashcan content with the given politic  
- restore - restore files and/or directories from trashcan  
- view - view trashcan contents  

`rmtrm` can be used as a shortcut to `rmt remove`.

## INSTALL ##

`python setup.py install`

## USAGE ##

Get a help message:

`rmt --help`
`rmt restore --help`


Remove a file:

`rmt remove file`
or
`rmtrm file`


Remove all \*.txt files from directory:

`rmt remove-regex dir "^.*\.txt$"`

View trashcan contents:

`rmt view`


Erase record with id 1 from trashcan:

`rmt erase --by id 1`


Restore file from trashcan:

`rmt restore --by name file`


Erase all records from trashcan:

`rmt autoclean all`
