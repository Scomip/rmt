"""Trashcan module of rmt_async.

Holds AsyncTrashcan class.

"""
import logging
import threading
import multiprocessing

from collections import deque
from multiprocessing import Queue

from rmt.trashcan import Trashcan, RecordNotFoundError

logger = logging.getLogger(__name__)

ACTION_PUT = 1
ACTION_ERASE = 2
ACTION_RESTORE = 3

ACTION_FUNCS = {
    ACTION_PUT: Trashcan._put_to_trashcan,
    ACTION_ERASE: Trashcan._erase_from_trashcan,
    ACTION_RESTORE: Trashcan._restore_from_trashcan,
}


class AsyncTrashcan(Trashcan):
    """Set of files in a certain directory.

    Async version of rmt.Trashcan. Main difference is that
    this class allows to start multiple actions and fetch
    results later.

    All action methods (put, erase and restore) return
    action id, which can be used to identify action when
    its result is fetched.

    Methods:
        put - Put a file or directory into trashcan.
        erase - Erase a record and remove associated file or
            directory.
        restore - Erase a record and restore associated file or
            directory to its real location.
        save - Save information about AsyncTrashcan on drive.
        items - List of AsyncTrashcan contents.
        count - Number of records.
        get - Get a record by id.
        fetch - Get a result of previously started action.

    """
    def __init__(self, path, concurrency=2, initial_action_id=1):
        self.next_action_id = initial_action_id
        self.actions = {}
        self.lock = threading.RLock()
        self.action_queue = Queue()
        self.result_queue = Queue()
        self.fetch_queue = deque()
        t = threading.Thread(target=self._result_handler)
        t.daemon = True
        super(AsyncTrashcan, self).__init__(path)
        self.record_flags = {
            record_id: True for record_id in self.records.iterkeys()
        }
        t.start()
        for _ in xrange(concurrency):
            process = multiprocessing.Process(
                None,
                name="AsyncTrashcan action handler",
                target=self._action_handler,
                args=(self.action_queue, self.result_queue)
            )
            process.daemon = True
            process.start()

    def put(self, path):
        """Put a file into the trashcan.

        Args:
            path: Path to a file or directory.

        Returns:
            action_id: integer which can be used to identify
                action. Unique for every action in session.

        """
        with self.lock:
            logger.info("putting %s into trashcan (%s)", path, self.path)
            record_id = self.next_id
            self.next_id += 1
            return self._create_action(
                ACTION_PUT,
                record_id,
                (path, self.path, record_id)
            )

    def erase(self, target_id):
        """Erase a file and the record with the target id.

        Args:
            target_id: Id of a record to erase.

        Returns:
            action_id: integer which can be used to identify
                action. Unique for every action in session.

        Raises:
            RecordNotFoundError.

        """
        with self.lock:
            logger.info("erasing record %i from trashcan (%s)", target_id,
                        self.path)
            if (target_id not in self.records or
                    not self.record_flags[target_id]):
                raise RecordNotFoundError(target_id)
            self.record_flags[target_id] = False
            return self._create_action(
                ACTION_ERASE,
                target_id,
                (self.path, target_id)
            )

    def restore(self, target_id):
        """Restore a file from the record with the target id.

        Args:
            target_id: Id of a record to restore.

        Returns:
            action_id: integer which can be used to identify
                action. Unique for every action in session.

        Raises:
            RecordNotFoundError.

        """
        with self.lock:
            logger.info("restoring record %i from trashcan (%s)",
                        target_id, self.path)
            if (target_id not in self.records or
                    not self.record_flags[target_id]):
                raise RecordNotFoundError(target_id)
            self.record_flags[target_id] = False
            record = self.records[target_id]
            return self._create_action(
                ACTION_RESTORE,
                target_id,
                (self.path, target_id, record.path, record.size)
            )

    def save(self):
        """Save trashcan contents."""
        with self.lock:
            super(AsyncTrashcan, self).save()

    def items(self):
        """Iterate on all trashcan record, sorted by id."""
        with self.lock:
            items = super(AsyncTrashcan, self).items()
        for item in items:
            with self.lock:
                if self.record_flags[item.record_id]:
                    yield item

    def count(self):
        """Return a number of records."""
        with self.lock:
            return super(AsyncTrashcan, self).count()

    def get(self, record_id):
        """Return a record with the target id."""
        with self.lock:
            return super(AsyncTrashcan, self).get(record_id)

    def fetch(self):
        """Fetch a result of previously started action.

        Returns:
            None: if no finished actions are available.
            (action_id, fetch_result): where
                action_id: unique integer for every action
                    in session.
                fetch_result: either TrashRecord or some Exception.

        """
        with self.lock:
            return self.fetch_queue.popleft() if self.fetch_queue else None

    def _create_action(self, action_type, record_id, args):
        action_id = self.next_action_id
        self.next_action_id += 1
        self.actions[action_id] = (action_type, record_id)
        self.action_queue.put((action_id, action_type, args))
        return action_id

    def _remove_record(self, record_id):
        with self.lock:
            del self.record_flags[record_id]
            return super(AsyncTrashcan, self)._remove_record(record_id)

    @staticmethod
    def _action_handler(action_queue, result_queue):
        while True:
            action_id, action_type, args = action_queue.get()
            try:
                result = ACTION_FUNCS[action_type](*args)
            except Exception as e:
                result = e
            result_queue.put((action_id, result))

    def _result_handler(self):
        while True:
            action_id, result = self.result_queue.get()
            with self.lock:
                action_type, record_id = self.actions[action_id]
                if isinstance(result, Exception):
                    if action_type in (ACTION_ERASE, ACTION_RESTORE):
                        self.record_flags[record_id] = True
                    fetch_result = result
                else:
                    if action_type == ACTION_PUT:
                        record = result
                        self.records[record_id] = record
                        self.record_flags[record_id] = True
                    elif action_type in (ACTION_ERASE, ACTION_RESTORE):
                        record = self._remove_record(record_id)
                    fetch_result = record

                self.fetch_queue.append((action_id, fetch_result))
