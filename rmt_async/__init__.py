"""rmt_async package.

Classes:
    AsyncTrashcan - async version of rmt.Trashcan.

"""
from rmt_async.trashcan import AsyncTrashcan

__all__ = ["AsyncTrashcan"]
